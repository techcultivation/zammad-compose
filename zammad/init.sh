#!/bin/bash

# init db
echo "migrating db"
bundle exec rake db:migrate
echo "seeding db"
bundle exec rake db:seed

# init elasticsearch
if [[ ! -z $ES_URL ]]; then
  echo "configuring elasticsearch"
  bundle exec rails r "Setting.set('es_url', '${ES_URL}')"
  echo "rebuilding index"
  bundle exec rake searchindex:rebuild
else
  echo "skipping elasticsearch setup (set ES_URL to enable)"
fi

echo "done"
