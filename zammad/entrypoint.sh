#!/bin/bash

# rails not cleaning up behind itself
rm -f tmp/pids/*

# write database config
sed \
  -e "s#.*adapter:.*#  adapter: postgresql#g" \
  -e "s#.*username:.*#  username: ${POSTGRESQL_USER:=postgres}#g" \
  -e "s#.*database:.*#  database: ${POSTGRESQL_DB:=postgres}#g" \
  -e "s#.*password:.*#  password: ${POSTGRESQL_PASS}\n  host: ${POSTGRESQL_HOST:=postgres}\n#g" \
  < config/database.yml.pkgr > config/database.yml

# enable memcached
sed -i -e "s/.*config.cache_store.*file_store.*cache_file_store.*/    config.cache_store = :dalli_store, '${MEMCACHED_HOST:=memcached}:11211'\n    config.session_store = :dalli_store, '${MEMCACHED_HOST:=memcached}:11211'/" config/application.rb

exec "$@"
