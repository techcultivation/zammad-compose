FROM ruby:2.4.2-slim

ENV ZAMMAD_VERSION 2.3.0
ENV ZAMMAD_DIR /opt/zammad
ENV RAILS_ENV production
ENV RAILS_SERVE_STATIC_FILES true
ENV ZAMMAD_URL https://ftp.zammad.com/zammad-${ZAMMAD_VERSION}.tar.gz

# install dependencies
RUN BUILD_DEPENDENCIES="build-essential ca-certificates wget git libffi-dev libpq5 libpq-dev" \
	&& apt-get update && apt-get install -y --force-yes --no-install-recommends ${BUILD_DEPENDENCIES} && rm -rf /var/lib/apt/lists/*

# install zammad
RUN wget ${ZAMMAD_URL} \
	&& mkdir ${ZAMMAD_DIR} \
	&& tar -xzf zammad-${ZAMMAD_VERSION}.tar.gz -C ${ZAMMAD_DIR} \
	&& rm zammad-${ZAMMAD_VERSION}.tar.gz \
	&& cd ${ZAMMAD_DIR} \
	&& bundle install --without test development mysql \
	&& contrib/packager.io/fetch_locales.rb \
	&& sed -e 's#.*adapter: postgresql#  adapter: nulldb#g' < config/database.yml.pkgr > config/database.yml \
	&& bundle exec rake assets:precompile \
	&& rm config/database.yml && rm -r tmp/cache

# symlink log files to stdout
RUN rm ${ZAMMAD_DIR}/log/production.log \
	&& ln -s /dev/stdout ${ZAMMAD_DIR}/log/production.log \
	&& ln -s /dev/stdout ${ZAMMAD_DIR}/log/websocket-server_out.log \
	&& ln -s /dev/stderr ${ZAMMAD_DIR}/log/websocket-server_err.log \
	&& ln -s /dev/stdout ${ZAMMAD_DIR}/log/scheduler_out.log \
	&& ln -s /dev/stderr ${ZAMMAD_DIR}/log/scheduler_err.log

WORKDIR ${ZAMMAD_DIR}
EXPOSE 3000 6042

COPY entrypoint.sh ${ZAMMAD_DIR}/entrypoint.sh
COPY init.sh ${ZAMMAD_DIR}/init.sh

ENTRYPOINT ["./entrypoint.sh"]
CMD ["bundle", "exec", "rails", "s"]
