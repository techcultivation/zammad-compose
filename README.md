# Zammad compose

* [upstream](https://github.com/zammad/zammad-docker-compose/blob/master/docker-compose.yml)

## version mapping

| git/docker tag | zammad | elastic |
| ---            | ---    | ---     |
| v1.0.0         | 2.3.0  | 5.6.7   |

## usage

* create data dir for elastic `mkdir -p data/elasticsearch && chown 1000:1000 data/elasticsearch`
* init db and elasticsearch `docker-compose run --rm zammad ./init.sh`
* `docker-compose up -d`

## todo

* drop user privileges
* [add expire headers](https://github.com/zammad/zammad/blob/develop/contrib/nginx/zammad.conf#L26)
